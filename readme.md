# Handbuch

1. In diesem Verzeichnis muss eine "config.php" angelegt werden, welche ein Array zurückgibt:
```php
<?php
// Für Benutzerdaten einen weiteren Eintrag in das Array einfügen

return [
    [
        'username' => 'Benutzer 1',
        'password' => 'Passwort 1',
    ],
    [
        'username' => 'Benutzer 2',
        'password' => 'Passwort 2',
    ],
];
```   

2. Es wird in diesem Verzeichnis ein Verzeichnis "_backups" angelegt und dort
werden Backups geschrieben (DB und/oder FTP)