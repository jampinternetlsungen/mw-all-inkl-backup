<?php

namespace MW\AllInkl\Backup\Backup;

class BackupFtp extends Backup
{
    private $ignore;
    private $source;
    /**
     * Anzahl der Backups, die gespeichert werden sollen, bevor das älteste gelöscht wird
     *
     * @var int
     */
    private $maxFiles = 7;
    /**
     * Wielange darf ein Backup auf dem Webserver existieren
     *
     * @var float|int
     */
    private $backupTime = 60 * 60 * 24 * 7; // 7 Tage

    /**
     * BackupFtp constructor.
     * @param Backup $backup
     * @param array $ignore
     * @param $source
     */
    public function __construct(array $ignore, $source)
    {
        $this->ignore = $ignore;
        $this->source = $source;
    }

    /**
     * @param $maxFiles
     */
    public function setMaxFiles($maxFiles)
    {
        $this->maxFiles = $maxFiles;
    }

    /**
     * @param $time
     */
    public function setBackupTime($time)
    {
        $this->backupTime = $time;
    }

    /**
     * @param $destination
     * @param string $excludePath
     * @return array
     */
    public function backup($destination, $excludePath = '')
    {
        include "Archive/Tar.php";

        $files = glob($this->source . '/*');

        $done = [];

        foreach ($files as $file) {
            // Alles überspringen, was in $this->ignore steht
            if (in_array($file, $this->ignore)) {
                continue;
            }

            // Keine Dateien backupen
            if (is_file($file)) {
                continue;
            }
//            echo $file."<br>";

            $folder = str_replace($this->source . '/', '', $file);

            $this->createDirIfNotExists($destination . '/' . $folder, 0744, true);

            $filename = 'backup_' . date('Y-m-d_H.i.s') . '_' . $folder;

            $tar = new \Archive_Tar($destination . '/' . $folder . '/' . $filename . '.tar.gz', true);
//            $tar->setIgnoreList($ignorieren);
            $tar->createModify($file, '', $excludePath);

            $done[] = $folder;
        }

        return $done;
    }

    /**
     * @param $destination
     */
    public function removeOld($destination)
    {
        $now = time();

        $files = glob($destination . '/*');

        foreach ($files as $file) {
            $files2 = glob($file . '/*');

            $numFiles = count($files2);

            if ($numFiles > $this->maxFiles) {
                foreach ($files2 as $file2) {
                    $filemtime = filemtime($file2);

    //                echo $file2 . '=' . date('d.m.Y H:i:s', $filemtime) ."<br>";

                    if ($now - $filemtime >= $this->backupTime) {
    //                    echo 'Datei ' . $file2 . ' muss gelöscht werden.';
                        unlink($file2);
                    }
                }
            }
        }
    }
}