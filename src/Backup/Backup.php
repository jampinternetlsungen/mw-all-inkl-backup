<?php

namespace MW\AllInkl\Backup\Backup;

class Backup
{
    /**
     * @param $dir
     * @param $chmod
     * @param false $recursively
     */
    protected function createDirIfNotExists($dir, $chmod = 0644, $recursively = false)
    {
        if (!is_dir($dir)) {
            mkdir($dir, $chmod, $recursively);
        }
    }
}