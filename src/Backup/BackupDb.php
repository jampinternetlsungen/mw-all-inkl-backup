<?php

namespace MW\AllInkl\Backup\Backup;

class BackupDb extends Backup
{
    private $databases;
    private $backupTime = 60 * 60 * 24 * 14; // 62 Tage

    /**
     * BackupDb constructor.
     * @param Backup $backup
     * @param array $databases
     */
    public function __construct(array $databases)
    {
        $this->databases = $databases;
    }

    /**
     * @param $time
     */
    public function setBackupTime($time)
    {
        $this->backupTime = $time;
    }

    /**
     * @param $destination
     * @return array
     */
    public function backup($destination)
    {
        $done = [];

        foreach ($this->databases as $database) {
            $this->createDirIfNotExists($destination . '/' . $database['database_name'], 0744, true);

            $filename = 'backup_' . date('Y-m-d_H.i.s') . '_' . $database['database_name'];

            $sqlFile = $destination . '/' . $database['database_name'] . '/' . $filename;

            $exec = 'mysqldump ';
            $exec .= ' -u ' . $database['database_name'];
            $exec .= ' -p"' . $database['database_password'] . '"';
            $exec .= ' --allow-keywords';
            $exec .= ' --add-drop-table';
            $exec .= ' --complete-insert';
            $exec .= ' --quote-names';
            $exec .= ' ' . $database['database_name'];
            $exec .= ' > ';
            $exec .= ' ' . $sqlFile . '.sql ';

            exec($exec);

//            exec("mysqldump -u " . $dbName . " -p'" . $dbPassword . "' --allow-keywords --add-drop-table --complete-insert --quote-names " . $dbName . " > ". $sqlFile . ".sql");
            exec('gzip ' . $sqlFile . '.sql');

            $done[] = $database['database_name'];
        }

        return $done;
    }

    /**
     * @param $destination
     */
    public function removeOld($destination)
    {
        $now = time();

        foreach ($this->databases as $database) {
            $files = glob($destination . '/' . $database['database_name'] . '/*');

            foreach ($files as $file) {
                $filemtime = filemtime($file);

//                echo $file . '=' . date('d.m.Y H:i:s', $filemtime) ."<br>";

                if ($now - $filemtime >= $this->backupTime) {
//                    echo 'Datei ' . $file . ' muss gelöscht werden.';
                    unlink($file);
                }
            }
        }
    }
}