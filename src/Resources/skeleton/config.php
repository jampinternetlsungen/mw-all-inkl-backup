<?php
/**
 * Put your configurations in here
 *
 * return [
 *      [
 *          'username' => 'User 1',
 *          'password' => 'Password 1',
 *          'jobs' => ['db', 'ftp'],
 *          'databases' => [
 *              [
 *                  'database_name' => 'Datenbank_1',
 *                  'database_password' => 'Passwort_1'
 *              ],
 *              [
 *                  'database_name' => 'Datenbank_2',
 *                  'database_password' => 'Passwort_2'
 *              ]
 *          ]
 *      ],
 * ];
 */
return [];