<?php
use MW\AllInkl\Backup\Backup;

@ini_set('max_execution_time', 3600);
@ini_set('memory_limit', '4G');

/** @var Composer\Autoload\ClassLoader $loader */
$loader = include_once 'vendor/autoload.php';

$rootPath = dirname(dirname(__FILE__));

try {
    $backup = new Backup($rootPath);
    $backup->setFtpBackupRoot($rootPath . '/..');
    $backup->run();
} catch (Exception $exception) {
    echo $exception->getMessage();
}