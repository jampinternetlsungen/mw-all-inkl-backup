<?php

namespace MW\AllInkl\Backup;

use KasApi\KasApi;
use KasApi\KasConfiguration;
use MW\AllInkl\Backup\Backup\BackupDb;
use MW\AllInkl\Backup\Backup\BackupFtp;

class Backup
{
    private $rootPath;
    private $ftpBackupRoot;

    public function __construct($rootPath)
    {
        $this->rootPath = $rootPath;
        $this->ftpBackupRoot = $rootPath . '/..';
    }

    public function setFtpBackupRoot($backupRoot)
    {
        $this->ftpBackupRoot = $backupRoot;
    }

    public function run()
    {
        $configFile = $this->rootPath . '/config.php';

        if (!file_exists($configFile)) {
            throw new \Exception('config file does not exist.');
        }

        if (file_exists($configFile)) {
            $configs = include_once $configFile;

            if (!is_array($configs)) {
                throw new \Exception('Config does not return an array.');
            }

            foreach ($configs as $config) {
                $username = $config['username'];
                $password = $config['password'];
                $jobs = $config['jobs'] ?? [];
                $databases = $config['databases'];

                if (!$username || !$password) {
                    throw new \Exception('Username or password empty.');
                }

                // DB sichern
                if ((empty($jobs) || in_array('db', $jobs)) && !empty($databases)) {
                    $backupDB = new BackupDb($databases);
                    $done = $backupDB->backup($this->rootPath . '/_backups/db');
                    $backupDB->removeOld($this->rootPath . '/_backups/db');
                }

                // FTP sichern
                if (empty($jobs) || in_array('ftp', $jobs)) {
                    $backupServer = new BackupFtp(
                        [
                            $this->ftpBackupRoot . '/.',
                            $this->ftpBackupRoot . '/..',
                            $this->ftpBackupRoot . '/!backup',
                            $this->ftpBackupRoot . '/cgi-bin',
                            $this->ftpBackupRoot . '/logs',
                        ],
                        $this->ftpBackupRoot
                    );
                    $backupServer->backup($this->rootPath . '/_backups/ftp');
                    $backupServer->removeOld($this->rootPath . '/_backups/ftp');
                }
            }
        }
    }
}