<?php

namespace MW\AllInkl\Backup\Composer;

use Composer\Script\Event;
use Composer\Util\Filesystem;
use Symfony\Component\Finder\Finder;


class ScriptHandler
{
    /**
     * Runs all Composer tasks to initialize a Contao Managed Edition.
     */
    public static function initializeApplication(Event $event)
    {
        $rootPath = self::getRootDir($event);
        $webDir = $rootPath . '/web';

        self::installIndex($webDir);
        self::installConfig($rootPath);
    }

    /**
     * @param $webDir
     */
    private static function installIndex($webDir)
    {
        $finder = Finder::create()->files()->in(__DIR__ . '/../Resources/skeleton/web');
        $fs = new Filesystem();

        foreach ($finder as $file) {
            $fs->ensureDirectoryExists($webDir);
            $fs->copy($file->getPathname(), $webDir . '/' . $file->getRelativePathname());
        }
    }

    /**
     * @param $rootPath
     */
    private static function installConfig($rootPath)
    {
        $finder = Finder::create()->files()->in(__DIR__ . '/../Resources/skeleton');
        $fs = new Filesystem();

        foreach ($finder as $file) {
            if (!file_exists($rootPath . '/' . $file->getRelativePathname())) {
                $fs->copy($file->getPathname(), $rootPath . '/' . $file->getRelativePathname());
            }
        }
    }

    /**
     * @param Event $event
     * @return string
     */
    private static function getRootDir(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');

        return dirname($vendorDir);
    }
}